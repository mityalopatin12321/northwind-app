﻿using AutoMapper;
using Northwind.Services.Employees;
using Northwind.Services.Products;
using Northwind.Services.Blogging;
using Northwind.Services.EntityFrameworkCore.Blogging.Entities;

namespace Northwind.Services.SQL
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            this.CreateMap<Product, EntityFrameworkCore_.Entities.Product> ();
            this.CreateMap< EntityFrameworkCore_.Entities.Product, Product >();

            this.CreateMap<ProductCategory, EntityFrameworkCore_.Entities.Category>();
            this.CreateMap<EntityFrameworkCore_.Entities.Category, ProductCategory>();

            this.CreateMap<Employee, EntityFrameworkCore_.Entities.Employee>();
            this.CreateMap<EntityFrameworkCore_.Entities.Employee, Employee>();

            this.CreateMap<BlogArticle, BlogArticleEntity>();
            this.CreateMap<BlogArticleEntity, BlogArticle>();

            this.CreateMap<BlogArticleProduct, BlogArticleProductEntity>();
            this.CreateMap<BlogArticleProductEntity, BlogArticleProduct>();

            this.CreateMap<BlogComment, BlogCommentEntity>();
            this.CreateMap<BlogCommentEntity, BlogComment>();
        }
    }
}
