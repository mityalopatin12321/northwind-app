﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Northwind.Services.EntityFrameworkCore.Context;
using Northwind.Services.Employees;
using AutoMapper;

namespace Northwind.Services.SQL.Employees
{
    public class EmployeeManagmentService : IEmployeeManagementService
    {
        private const int Reserved = 78;
        private readonly NorthwindContext context;
        private readonly IMapper mapper;

        public EmployeeManagmentService(NorthwindContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<int> CreateEmployeeAsync(Employee employee)
        {
            this.context.Employees.Add(this.mapper.Map<EntityFrameworkCore_.Entities.Employee>(employee));
            int rowsAffected = await this.context.SaveChangesAsync();
            return rowsAffected;
        }

        public async Task<bool> DeleteEmployeeAsync(int employeeId)
        {
            var category = await this.context.Employees.FindAsync(employeeId);
            if (category is null)
            {
                return false;
            }

            this.context.Employees.Remove(category);
            await this.context.SaveChangesAsync();
            return true;
        }

        public async Task<bool> DeletePictureAsync(int employeeId)
        {
            var employee = await this.context.Employees.FindAsync(employeeId);
            if (employee is null)
            {
                return false;
            }

            employee.Photo = null;
            await this.context.SaveChangesAsync();
            return true;
        }

        public async IAsyncEnumerable<Employee> GetEmployeesAsync()
        {
            await foreach (var employee in this.context.Employees.AsAsyncEnumerable())
            {
                yield return this.mapper.Map<Employee>(employee);
            }
        }

        public async Task<(bool, Employee)> TryGetEmployeeAsync(int employeeId)
        {
            var employee = await this.context.Employees.FindAsync(employeeId);
            if (employee is null)
            {
                return (false, null);
            }

            return (true, this.mapper.Map<Employee>(employee));
        }

        public async Task<(bool, byte[])> TryGetPictureAsync(int employeeId)
        {
            var employee = await this.context.Employees.FindAsync(employeeId);
            if (employee is null)
            {
                return (false, null);
            }
            if (employee.Photo is null)
            {
                return (false, null);
            }

            return (true, employee.Photo[Reserved..]);
        }

        public async Task<bool> UpdateEmployeeAsync(int employeeId, Employee employee)
        {
            var upd = await this.context.Employees.FindAsync(employeeId);

            if (upd is null)
            {
                return false;
            }

            upd.LastName = employee.LastName;
            upd.FirstName = employee.FirstName;
            upd.Title = employee.Title;
            upd.TitleOfCourtesy = employee.TitleOfCourtesy;
            upd.BirthDate = employee.BirthDate;
            upd.HireDate = employee.HireDate;
            upd.Address = employee.Address;
            upd.City = employee.City;
            upd.Region = employee.Region;
            upd.PostalCode = employee.PostalCode;
            upd.Country = employee.Country;
            upd.HomePhone = employee.HomePhone;
            upd.Extension = employee.Extension;
            upd.Photo = employee.Photo;
            upd.Notes = employee.Notes;
            upd.ReportsTo = employee.ReportsTo;
            upd.PhotoPath = employee.PhotoPath;

            await this.context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> UpdatePictureAsync(int employeeId, Stream stream)
        {
            var upd = await this.context.Employees.FindAsync(employeeId);

            if (upd is null)
            {
                return false;
            }

            upd.Photo = new byte[stream.Length + Reserved];
            await stream.ReadAsync(upd.Photo, Reserved, (int)stream.Length);

            await this.context.SaveChangesAsync();

            return true;
        }
    }
}
