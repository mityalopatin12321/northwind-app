﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Northwind.Services.EntityFrameworkCore.Context;
using Northwind.Services.Products;
using AutoMapper;

namespace Northwind.Services.SQL.Products
{
    /// <summary>
    /// Represents a stub for a product management service.
    /// </summary>
    public sealed class ProductManagementService : IProductManagementService
    {
        private readonly NorthwindContext context;
        private readonly IMapper mapper;

        public ProductManagementService(NorthwindContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <inheritdoc/>
        public async Task<int> CreateProductAsync(Product product)
        {
            this.context.Products.Add(this.mapper.Map<EntityFrameworkCore_.Entities.Product>(product));
            int rowsAffected = await this.context.SaveChangesAsync();
            return rowsAffected;
        }

        /// <inheritdoc/>
        public async Task<bool> DeleteProductAsync(int productId)
        {
            var product = await this.context.Products.FindAsync(productId);
            if (product is null)
            {
                return false;
            }

            this.context.Products.Remove(product);
            await this.context.SaveChangesAsync();
            return true;
        }

        /// <inheritdoc/>
        public async IAsyncEnumerable<Product> GetProductsAsync()
        {
            await foreach(var product in this.context.Products.AsAsyncEnumerable())
            {
                yield return this.mapper.Map<Product>(product);
            }
        }

        /// <inheritdoc/>
        public async Task<(bool, Product)> TryGetProductAsync(int productId)
        {
            var product = await this.context.Products.FindAsync(productId);
            if(product is null)
            {
                return (false, null);
            }

            return (true, this.mapper.Map<Product>(product));
        }

        /// <inheritdoc/>
        public async Task<bool> UpdateProductAsync(int productId, Product product)
        {
            var upd = await this.context.Products.FindAsync(productId);

            if (upd is null)
            {
                return false;
            }

            upd.Discontinued = product.Discontinued;
            upd.CategoryId = product.CategoryId;
            upd.ProductName = product.ProductName;
            upd.QuantityPerUnit = product.QuantityPerUnit;
            upd.ReorderLevel = product.ReorderLevel;
            upd.SupplierId = product.SupplierId;
            upd.UnitPrice = product.UnitPrice;
            upd.UnitsInStock = product.UnitsInStock;
            upd.UnitsOnOrder = product.UnitsOnOrder;

            await this.context.SaveChangesAsync();

            return true;
        }
    }
}
