﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Northwind.Services.EntityFrameworkCore.Context;
using Northwind.Services.Products;
using AutoMapper;

namespace Northwind.Services.SQL.Products
{
    public class ProductCategoriesManagementService : IProductCategoriesManagementService
    {
        private const int Reserved = 78;
        private readonly NorthwindContext context;
        private readonly IMapper mapper;

        public ProductCategoriesManagementService(NorthwindContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        /// <inheritdoc/>
        public async Task<int> CreateCategoryAsync(ProductCategory productCategory)
        {
            this.context.Categories.Add(this.mapper.Map<EntityFrameworkCore_.Entities.Category>(productCategory));
            int rowsAffected = await this.context.SaveChangesAsync();
            return rowsAffected;
        }

        /// <inheritdoc/>
        public async Task<bool> DeleteCategoryAsync(int categoryId)
        {
            var category = await this.context.Categories.FindAsync(categoryId);
            if (category is null)
            {
                return false;
            }

            this.context.Categories.Remove(category);
            await this.context.SaveChangesAsync();
            return true;
        }

        /// <inheritdoc/>
        public async Task<bool> DeletePictureAsync(int categoryId)
        {
            var category = await this.context.Categories.FindAsync(categoryId);
            if (category is null)
            {
                return false;
            }

            category.Picture = null;
            await this.context.SaveChangesAsync();
            return true;
        }

        /// <inheritdoc/>
        public async IAsyncEnumerable<ProductCategory> GetCategoriesAsync()
        {
            await foreach(var category in this.context.Categories.AsAsyncEnumerable())
            {
                yield return this.mapper.Map<ProductCategory>(category);
            }
        }

        /// <inheritdoc/>
        public async Task<(bool, ProductCategory)> TryGetCategoryAsync(int categoryId)
        {
            var category = await this.context.Categories.FindAsync(categoryId);
            if (category is null)
            {
                return (false, null);
            }

            return (true, this.mapper.Map<ProductCategory>(category));
        }

        /// <inheritdoc/>
        public async Task<(bool, byte[])> TryGetPictureAsync(int categoryId)
        {
            var category = await this.context.Categories.FindAsync(categoryId);
            if (category is null)
            {
                return (false, null);
            }
            if(category.Picture is null)
            {
                return (false, null);
            }

            return (true, category.Picture[Reserved..]);
        }

        /// <inheritdoc/>
        public async Task<bool> UpdateCategoryAsync(int categoryId, ProductCategory productCategory)
        {
            var upd = await this.context.Categories.FindAsync(categoryId);

            if (upd is null)
            {
                return false;
            }

            upd.Description = productCategory.Description;
            upd.CategoryName = productCategory.CategoryName;

            await this.context.SaveChangesAsync();

            return true;
        }

        /// <inheritdoc/>
        public async Task<bool> UpdatePictureAsync(int categoryId, Stream stream)
        {
            var upd = await this.context.Categories.FindAsync(categoryId);

            if (upd is null)
            {
                return false;
            }

            upd.Picture = new byte[stream.Length + Reserved];
            await stream.ReadAsync(upd.Picture, Reserved, (int)stream.Length);

            await this.context.SaveChangesAsync();

            return true;
        }
    }
}
