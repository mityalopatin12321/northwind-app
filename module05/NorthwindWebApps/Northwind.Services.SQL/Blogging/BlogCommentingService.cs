﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Northwind.Services.EntityFrameworkCore.Blogging.Context;
using Northwind.Services.EntityFrameworkCore.Blogging.Entities;
using Northwind.Services.Blogging;
using AutoMapper;

namespace Northwind.Services.SQL.Blogging
{
    public class BlogCommentingService : IBlogCommentingService
    {
        private readonly BloggingContext context;
        private readonly IMapper mapper;

        public BlogCommentingService(BloggingContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public async Task<int> CreateBlogCommentAsync(BlogComment blogComment)
        {
            this.context.BlogComments.Add(this.mapper.Map<BlogCommentEntity>(blogComment));
            int rowsAffected = await this.context.SaveChangesAsync();
            return rowsAffected;
        }

        public async Task<bool> DeleteBlogCommentAsync(int blogCommentId)
        {
            var blogComment = await this.context.BlogComments.FindAsync(blogCommentId);
            if (blogComment is null)
            {
                return false;
            }

            this.context.BlogComments.Remove(blogComment);
            await this.context.SaveChangesAsync();
            return true;
        }

        public async IAsyncEnumerable<BlogComment> GetBlogCommentAsync()
        {
            await foreach (var blogComment in this.context.BlogComments.AsAsyncEnumerable())
            {
                yield return this.mapper.Map<BlogComment>(blogComment);
            }
        }

        public async Task<(bool, BlogComment)> TryGetBlogCommentAsync(int blogCommentId)
        {
            var blogComment = await this.context.BlogComments.FindAsync(blogCommentId);
            if (blogComment is null)
            {
                return (false, null);
            }

            return (true, this.mapper.Map<BlogComment>(blogComment));
        }

        public async Task<bool> UpdateBlogCommentAsync(int blogCommentId, BlogComment blogComment)
        {
            var upd = await this.context.BlogComments.FindAsync(blogCommentId);

            if (upd is null)
            {
                return false;
            }

            upd.Text = blogComment.Text;

            await this.context.SaveChangesAsync();

            return true;
        }
    }
}
