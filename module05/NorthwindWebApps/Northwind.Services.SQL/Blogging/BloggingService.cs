﻿using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using Northwind.Services.EntityFrameworkCore.Blogging.Context;
using Northwind.Services.EntityFrameworkCore.Blogging.Entities;
using Northwind.Services.Employees;
using Northwind.Services.Blogging;
using Northwind.Services.Products;
using AutoMapper;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Northwind.Services.SQL.Blogging
{
    public class BloggingService : IBloggingService
    {
        private readonly BloggingContext context;
        private readonly IEmployeeManagementService employeeManagementService;
        private readonly IProductManagementService productManagementService;
        private readonly IBlogCommentingService blogCommentingService;
        private readonly IMapper mapper;

        public BloggingService(BloggingContext context, IEmployeeManagementService employeeManagementService,IProductManagementService productManagementService, IBlogCommentingService blogCommentingService, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
            this.employeeManagementService = employeeManagementService;
            this.productManagementService = productManagementService;
            this.blogCommentingService = blogCommentingService;
        }

        public async Task<int> CreateBlogArticleAsync(BlogArticle blogArticle)
        {
            (bool exist, Employee employee) = await this.employeeManagementService.TryGetEmployeeAsync(blogArticle.EmployeeId);
            if(!exist)
            {
                return -1;
            }

            blogArticle.PublishDate = DateTime.Now;

            this.context.BlogArticles.Add(this.mapper.Map<BlogArticleEntity>(blogArticle));
            int rowsAffected = await this.context.SaveChangesAsync();
            return rowsAffected;
        }

        public async Task<bool> DeleteBlogArticleAsync(int blogArticleId)
        {
            var blogArticle = await this.context.BlogArticles.FindAsync(blogArticleId);
            if (blogArticle is null)
            {
                return false;
            }

            this.context.Entry(blogArticle).Collection(x => x.BlogComments).Load();

            foreach(var comment in blogArticle.BlogComments)
            {
                await blogCommentingService.DeleteBlogCommentAsync(comment.CommentId);
            }

            this.context.BlogArticles.Remove(blogArticle);
            await this.context.SaveChangesAsync();
            return true;
        }

        public async IAsyncEnumerable<BlogArticle> GetBlogArticleAsync()
        {
            await foreach (var blogArticle in this.context.BlogArticles.AsAsyncEnumerable())
            {
                yield return this.mapper.Map<BlogArticle>(blogArticle);
            }
        }

        public async Task<(bool, BlogArticle)> TryGetBlogArticleAsync(int blogArticleId)
        {
            var blogArticle = await this.context.BlogArticles.FindAsync(blogArticleId);
            if (blogArticle is null)
            {
                return (false, null);
            }

            this.context.Entry(blogArticle).Collection(x => x.BlogArticleProducts).Load();
            this.context.Entry(blogArticle).Collection(x => x.BlogComments).Load();

            return (true, this.mapper.Map<BlogArticle>(blogArticle));
        }

        public async Task<bool> UpdateBlogArticleAsync(int blogArticleId, BlogArticle blogArticle)
        {
            var upd = await this.context.BlogArticles.FindAsync(blogArticleId);

            if (upd is null)
            {
                return false;
            }

            upd.PublishDate = DateTime.Now;
            upd.Title = blogArticle.Title;
            upd.Content = blogArticle.Content;

            await this.context.SaveChangesAsync();

            return true;
        }

        public async Task<int> LinkProductToBlogArticleAsync(int blogArticleId, int productId)
        {
            (bool flag, _) = await TryGetBlogArticleAsync(blogArticleId);
            if(!flag)
            {
                return -1;
            }

            (flag, _) = await productManagementService.TryGetProductAsync(productId);
            if (!flag)
            {
                return -1;
            }

            this.context.BlogArticleProducts.Add(new BlogArticleProductEntity() { BlogArticleId = blogArticleId, ProductId = productId });
            int rowsAffected = await this.context.SaveChangesAsync();
            return rowsAffected;
        }

        public async Task<bool> DeleteProductToBlogArticleLinkAsync(int blogArticleId, int productId)
        {
            var link = await this.context.BlogArticleProducts.Where(x => x.ProductId == productId && x.BlogArticleId == blogArticleId).FirstOrDefaultAsync();

            if(link is null)
            {
                return false;
            }

            this.context.BlogArticleProducts.Remove(link);
            await this.context.SaveChangesAsync();
            return true;
        }
    }
}
