﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Northwind.Services.EntityFrameworkCore.Blogging.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "BlogArticles",
                columns: table => new
                {
                    blog_article_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    title = table.Column<string>(type: "nvarchar(15)", maxLength: 15, nullable: true),
                    content = table.Column<string>(type: "nvarchar(1000)", maxLength: 1000, nullable: true),
                    publish_date = table.Column<DateTime>(type: "datetime", nullable: false),
                    employee_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogArticles", x => x.blog_article_id);
                });

            migrationBuilder.CreateTable(
                name: "BlogArticleProducts",
                columns: table => new
                {
                    record_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    blog_article_id = table.Column<int>(type: "int", nullable: false),
                    product_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogArticleProducts", x => x.record_id);
                    table.ForeignKey(
                        name: "FK_BlogArticleProducts_BlogArticles_blog_article_id",
                        column: x => x.blog_article_id,
                        principalTable: "BlogArticles",
                        principalColumn: "blog_article_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BlogComments",
                columns: table => new
                {
                    comment_id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    text = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    blog_article_id = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BlogComments", x => x.comment_id);
                    table.ForeignKey(
                        name: "FK_BlogComments_BlogArticles_blog_article_id",
                        column: x => x.blog_article_id,
                        principalTable: "BlogArticles",
                        principalColumn: "blog_article_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BlogArticleProducts_blog_article_id",
                table: "BlogArticleProducts",
                column: "blog_article_id");

            migrationBuilder.CreateIndex(
                name: "IX_BlogComments_blog_article_id",
                table: "BlogComments",
                column: "blog_article_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BlogArticleProducts");

            migrationBuilder.DropTable(
                name: "BlogComments");

            migrationBuilder.DropTable(
                name: "BlogArticles");
        }
    }
}
