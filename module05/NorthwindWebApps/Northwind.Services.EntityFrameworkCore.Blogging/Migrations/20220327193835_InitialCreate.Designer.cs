﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Northwind.Services.EntityFrameworkCore.Blogging.Context;

namespace Northwind.Services.EntityFrameworkCore.Blogging.Migrations
{
    [DbContext(typeof(BloggingContext))]
    [Migration("20220327193835_InitialCreate")]
    partial class InitialCreate
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("ProductVersion", "5.0.15")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Northwind.Services.EntityFrameworkCore.Blogging.Entities.BlogArticleEntity", b =>
                {
                    b.Property<int>("BlogArticleId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("blog_article_id")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Content")
                        .HasMaxLength(1000)
                        .HasColumnType("nvarchar(1000)")
                        .HasColumnName("content");

                    b.Property<int>("EmployeeId")
                        .HasColumnType("int")
                        .HasColumnName("employee_id");

                    b.Property<DateTime>("PublishDate")
                        .HasColumnType("datetime")
                        .HasColumnName("publish_date");

                    b.Property<string>("Title")
                        .HasMaxLength(15)
                        .HasColumnType("nvarchar(15)")
                        .HasColumnName("title");

                    b.HasKey("BlogArticleId");

                    b.ToTable("BlogArticles");
                });

            modelBuilder.Entity("Northwind.Services.EntityFrameworkCore.Blogging.Entities.BlogArticleProductEntity", b =>
                {
                    b.Property<int>("RecordId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("record_id")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("BlogArticleId")
                        .HasColumnType("int")
                        .HasColumnName("blog_article_id");

                    b.Property<int>("ProductId")
                        .HasColumnType("int")
                        .HasColumnName("product_id");

                    b.HasKey("RecordId");

                    b.HasIndex("BlogArticleId");

                    b.ToTable("BlogArticleProducts");
                });

            modelBuilder.Entity("Northwind.Services.EntityFrameworkCore.Blogging.Entities.BlogCommentEntity", b =>
                {
                    b.Property<int>("CommentId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasColumnName("comment_id")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<int>("BlogArticleId")
                        .HasColumnType("int")
                        .HasColumnName("blog_article_id");

                    b.Property<string>("Text")
                        .HasMaxLength(100)
                        .HasColumnType("nvarchar(100)")
                        .HasColumnName("text");

                    b.HasKey("CommentId");

                    b.HasIndex("BlogArticleId");

                    b.ToTable("BlogComments");
                });

            modelBuilder.Entity("Northwind.Services.EntityFrameworkCore.Blogging.Entities.BlogArticleProductEntity", b =>
                {
                    b.HasOne("Northwind.Services.EntityFrameworkCore.Blogging.Entities.BlogArticleEntity", "BlogArticle")
                        .WithMany("BlogArticleProducts")
                        .HasForeignKey("BlogArticleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("BlogArticle");
                });

            modelBuilder.Entity("Northwind.Services.EntityFrameworkCore.Blogging.Entities.BlogCommentEntity", b =>
                {
                    b.HasOne("Northwind.Services.EntityFrameworkCore.Blogging.Entities.BlogArticleEntity", "BlogArticle")
                        .WithMany("BlogComments")
                        .HasForeignKey("BlogArticleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("BlogArticle");
                });

            modelBuilder.Entity("Northwind.Services.EntityFrameworkCore.Blogging.Entities.BlogArticleEntity", b =>
                {
                    b.Navigation("BlogArticleProducts");

                    b.Navigation("BlogComments");
                });
#pragma warning restore 612, 618
        }
    }
}
