﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Northwind.Services.EntityFrameworkCore.Blogging.Entities
{
    [Table("BlogComments")]
    public class BlogCommentEntity
    {
        [Key]
        [Column("comment_id")]
        public int CommentId { get; set; }

        [Column("text")]
        [StringLength(100)]
        public string Text { get; set; }

        [Column("blog_article_id")]
        [Required]
        public int BlogArticleId { get; set; }

        [ForeignKey(nameof(BlogArticleId))]
        [InverseProperty("BlogComments")]
        public BlogArticleEntity BlogArticle { get; set; }
    }
}
