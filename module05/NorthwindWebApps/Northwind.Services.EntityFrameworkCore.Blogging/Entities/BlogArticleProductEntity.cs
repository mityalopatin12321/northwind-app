﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Northwind.Services.EntityFrameworkCore.Blogging.Entities
{
    [Table("BlogArticleProducts")]
    public class BlogArticleProductEntity
    {
        [Key]
        [Column("record_id")]
        public int RecordId { get; set; }

        [Column("blog_article_id")]
        public int BlogArticleId { get; set; }

        [Column("product_id")]
        public int ProductId { get; set; }

        [ForeignKey(nameof(BlogArticleId))]
        [InverseProperty("BlogArticleProducts")]
        public BlogArticleEntity BlogArticle { get; set; }
    }
}
