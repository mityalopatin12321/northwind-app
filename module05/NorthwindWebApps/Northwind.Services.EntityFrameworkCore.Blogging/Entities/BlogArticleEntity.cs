﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Northwind.Services.EntityFrameworkCore.Blogging.Entities
{
    [Table("BlogArticles")]
    public class BlogArticleEntity
    {
        [Key]
        [Column("blog_article_id")]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BlogArticleId { get; set; }

        [StringLength(15)]
        [Column("title")]
        public string Title { get; set; }

        [StringLength(1000)]
        [Column("content")]
        public string Content { get; set; }

        [Column("publish_date", TypeName = "datetime")]
        public DateTime PublishDate { get; set; }

        [Required]
        [Column("employee_id")]
        public int EmployeeId { get; set; }

        [InverseProperty(nameof(BlogCommentEntity.BlogArticle))]
        public virtual ICollection<BlogCommentEntity> BlogComments { get; set; }

        [InverseProperty(nameof(BlogArticleProductEntity.BlogArticle))]
        public virtual ICollection<BlogArticleProductEntity> BlogArticleProducts { get; set; }
    }
}
