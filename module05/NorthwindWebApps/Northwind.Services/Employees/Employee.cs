﻿using System;

namespace Northwind.Services.Employees
{
    /// <summary>
    /// Represents an employee.
    /// </summary>
    public class Employee
    {
        /// <summary>
        /// Gets or sets a product employee identifier.
        /// </summary>
        public int EmployeeId { get; set; }

        /// <summary>
        /// Gets or sets LastName.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets FirstName.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets Title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets TitleOfCourtesy.
        /// </summary>
        public string TitleOfCourtesy { get; set; }

        /// <summary>
        /// Gets or sets BirthDate.
        /// </summary>
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Gets or sets HireDate.
        /// </summary>
        public DateTime HireDate { get; set; }

        /// <summary>
        /// Gets or sets Address.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets City.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets Region.
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Gets or sets PostalCode.
        /// </summary>
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets Country.
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets HomePhone.
        /// </summary>
        public string HomePhone { get; set; }

        /// <summary>
        /// Gets or sets Extension.
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// Gets or sets a product category picture.
        /// </summary>
#pragma warning disable CA1819 // Properties should not return arrays
        public byte[] Photo { get; set; }
#pragma warning restore CA1819 // Properties should not return arrays

        /// <summary>
        /// Gets or sets Notes.
        /// </summary>
        public string Notes { get; set; }

        /// <summary>
        /// Gets or sets ReportsTo.
        /// </summary>
        public int ReportsTo { get; set; }

        /// <summary>
        /// Gets or sets PhotoPath.
        /// </summary>
        public string PhotoPath { get; set; }
    }
}
