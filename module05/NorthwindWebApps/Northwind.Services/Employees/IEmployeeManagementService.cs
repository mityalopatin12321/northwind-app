﻿using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace Northwind.Services.Employees
{
    /// <summary>
    /// Represents a management service for employees.
    /// </summary>
    public interface IEmployeeManagementService
    {
        /// <summary>
        /// Shows a list of employees.
        /// </summary>
        /// <returns>A <see cref="IList{T}"/> of <see cref="Employee"/>.</returns>
        IAsyncEnumerable<Employee> GetEmployeesAsync();

        /// <summary>
        /// Try to show a employeeId with specified identifier.
        /// </summary>
        /// <param name="employeeId">A employeeId identifier.</param>
        /// <returns>Returns true if a employeeId is returned; otherwise false.</returns>
        Task<(bool, Employee)> TryGetEmployeeAsync(int employeeId);

        /// <summary>
        /// Creates a new employee.
        /// </summary>
        /// <param name="employee">A <see cref="Employee"/> to create.</param>
        /// <returns>An identifier of a created employee.</returns>
        Task<int> CreateEmployeeAsync(Employee employee);

        /// <summary>
        /// Destroys an existed employee.
        /// </summary>
        /// <param name="employeeId">A employee identifier.</param>
        /// <returns>True if a employee is destroyed; otherwise false.</returns>
        Task<bool> DeleteEmployeeAsync(int employeeId);

        /// <summary>
        /// Updates a employeey.
        /// </summary>
        /// <param name="employeeId">A employee identifier.</param>
        /// <param name="employee">A <see cref="Employee"/>.</param>
        /// <returns>True if a employee is updated; otherwise false.</returns>
        Task<bool> UpdateEmployeeAsync(int employeeId, Employee employee);

        /// <summary>
        /// Try to show a employee picture.
        /// </summary>
        /// <param name="employeeId">A employee identifier.</param>
        /// <returns>True if a employee is exist; otherwise false.</returns>
        Task<(bool, byte[])> TryGetPictureAsync(int employeeId);

        /// <summary>
        /// Update a employee picture.
        /// </summary>
        /// <param name="employeeId">A employee identifier.</param>
        /// <param name="stream">A <see cref="Stream"/>.</param>
        /// <returns>True if a employee is exist; otherwise false.</returns>
        Task<bool> UpdatePictureAsync(int employeeId, Stream stream);

        /// <summary>
        /// Destroy a employee picture.
        /// </summary>
        /// <param name="employeeId">A employee identifier.</param>
        /// <returns>True if a employee is exist; otherwise false.</returns>
        Task<bool> DeletePictureAsync(int employeeId);
    }
}
