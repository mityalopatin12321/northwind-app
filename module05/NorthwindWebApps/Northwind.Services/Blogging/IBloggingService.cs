﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Northwind.Services.Products;

namespace Northwind.Services.Blogging
{
    /// <summary>
    /// Represents a management service for blogs.
    /// </summary>
    public interface IBloggingService
    {
        /// <summary>
        /// Shows a list of Blog Articles.
        /// </summary>
        /// <returns>A <see cref="IList{T}"/> of <see cref="BlogArticle"/>.</returns>
        IAsyncEnumerable<BlogArticle> GetBlogArticleAsync();

        /// <summary>
        /// Try to show a Blog Article with specified identifier.
        /// </summary>
        /// <param name="blogArticleId">A blogArticleId identifier.</param>
        /// <returns>Returns true if a blogArticleId is returned; otherwise false.</returns>
        Task<(bool, BlogArticle)> TryGetBlogArticleAsync(int blogArticleId);

        /// <summary>
        /// Creates a new Blog Article.
        /// </summary>
        /// <param name="blogArticle">A <see cref="BlogArticle"/> to create.</param>
        /// <returns>An identifier of a created blog Article.</returns>
        Task<int> CreateBlogArticleAsync(BlogArticle blogArticle);

        /// <summary>
        /// Destroys an existed Blog Article.
        /// </summary>
        /// <param name="blogArticleId">A blog Article identifier.</param>
        /// <returns>True if a blog Article is destroyed; otherwise false.</returns>
        Task<bool> DeleteBlogArticleAsync(int blogArticleId);

        /// <summary>
        /// Updates a Blog Article.
        /// </summary>
        /// <param name="blogArticleId">A Blog Article identifier.</param>
        /// <param name="blogArticle">A <see cref="BlogArticle"/>.</param>
        /// <returns>True if a Blog Article is updated; otherwise false.</returns>
        Task<bool> UpdateBlogArticleAsync(int blogArticleId, BlogArticle blogArticle);

        /// <summary>
        /// Links product to blog article.
        /// </summary>
        /// <param name="blogArticleId">A blog Article identifier.</param>
        /// <param name="productId">A product identifier.</param>
        /// <returns>An identifier of a created Link.</returns>
        Task<int> LinkProductToBlogArticleAsync(int blogArticleId, int productId);

        /// <summary>
        /// Delete Link of product to blog article.
        /// </summary>
        /// <param name="blogArticleId">A blog Article identifier.</param>
        /// <param name="productId">A product identifier.</param>
        /// <returns>True if a Link is destroyed; otherwise false.</returns>
        Task<bool> DeleteProductToBlogArticleLinkAsync(int blogArticleId, int productId);
    }
}
