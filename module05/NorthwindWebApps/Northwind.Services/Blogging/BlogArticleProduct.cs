﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.Services.Blogging
{
    public class BlogArticleProduct
    {
        public int RecordId { get; set; }

        public int BlogArticleId { get; set; }

        public int ProductId { get; set; }

        public BlogArticle BlogArticle { get; set; }
    }
}
