﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.Services.Blogging
{
    /// <summary>
    /// Represents a management service for commenting blogs.
    /// </summary>
    public interface IBlogCommentingService
    {
        /// <summary>
        /// Shows a list of Blog Comment.
        /// </summary>
        /// <returns>A <see cref="IList{T}"/> of <see cref="BlogComment"/>.</returns>
        IAsyncEnumerable<BlogComment> GetBlogCommentAsync();

        /// <summary>
        /// Try to show a Blog Comment with specified identifier.
        /// </summary>
        /// <param name="blogCommentId">A blogCommentId identifier.</param>
        /// <returns>Returns true if a blogCommentId is returned; otherwise false.</returns>
        Task<(bool, BlogComment)> TryGetBlogCommentAsync(int blogCommentId);

        /// <summary>
        /// Creates a new Blog Comment.
        /// </summary>
        /// <param name="blogComment">A <see cref="BlogComment"/> to create.</param>
        /// <returns>An identifier of a created blog Comment.</returns>
        Task<int> CreateBlogCommentAsync(BlogComment blogComment);

        /// <summary>
        /// Destroys an existed Blog Comment.
        /// </summary>
        /// <param name="blogCommentId">A blog Comment identifier.</param>
        /// <returns>True if a blog Comment is destroyed; otherwise false.</returns>
        Task<bool> DeleteBlogCommentAsync(int blogCommentId);

        /// <summary>
        /// Updates a Blog Comment.
        /// </summary>
        /// <param name="blogCommentId">A Blog Comment identifier.</param>
        /// <param name="blogComment">A <see cref="BlogComment"/>.</param>
        /// <returns>True if a Blog Comment is updated; otherwise false.</returns>
        Task<bool> UpdateBlogCommentAsync(int blogCommentId, BlogComment blogComment);
    }
}
