﻿using System;
using System.Collections.Generic;

namespace Northwind.Services.Blogging
{
    public class BlogArticle
    {
        public int BlogArticleId { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime PublishDate { get; set; }

        public int EmployeeId { get; set; }

        public ICollection<BlogComment> BlogComments { get; set; }

        public virtual ICollection<BlogArticleProduct> BlogArticleProducts { get; set; }
    }
}
