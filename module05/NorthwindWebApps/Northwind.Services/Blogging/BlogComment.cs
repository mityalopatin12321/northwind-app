﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Northwind.Services.Blogging
{
    public class BlogComment
    {
        public int CommentId { get; set; }

        public string Text { get; set; }

        public int BlogArticleId { get; set; }

        public BlogArticle BlogArticle { get; set; }
    }
}
