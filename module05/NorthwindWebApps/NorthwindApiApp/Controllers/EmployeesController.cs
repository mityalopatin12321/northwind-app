﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Northwind.Services.Employees;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NorthwindApiApp.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        private readonly IEmployeeManagementService employeeManagementService;

        public EmployeesController(IEmployeeManagementService employeeManagementService)
        {
            this.employeeManagementService = employeeManagementService;
        }

        // GET: <EmployeesController>
        [HttpGet]
        public IAsyncEnumerable<Employee> Get()
        {
            return this.employeeManagementService.GetEmployeesAsync();
        }

        // GET <EmployeesController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            (bool response, Employee category) = await this.employeeManagementService.TryGetEmployeeAsync(id);
            if (response)
            {
                return this.Ok(category);
            }

            return this.NotFound();
        }

        // POST <EmployeesController>
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Employee employee)
        {
            if (employee is null)
            {
                return this.BadRequest();
            }

            await this.employeeManagementService.CreateEmployeeAsync(employee);

            return this.Ok();
        }

        // PUT <EmployeesController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] Employee employee)
        {
            if (employee is null)
            {
                return this.BadRequest();
            }

            if (await this.employeeManagementService.UpdateEmployeeAsync(id, employee))
            {
                return this.Ok();
            }

            return this.NotFound();
        }

        // DELETE <EmployeesController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (await this.employeeManagementService.DeleteEmployeeAsync(id))
            {
                return this.Ok();
            }

            return this.NotFound();
        }

        // PUT <EmployeesController>/5/picture
        [HttpPut("{id}/picture")]
        public async Task<ActionResult> PutPicture(int id, IFormFile image)
        {
            if (image is null)
            {
                return this.BadRequest();
            }

            if (await this.employeeManagementService.UpdatePictureAsync(id, image.OpenReadStream()))
            {
                return this.Ok();
            }

            return this.NotFound();
        }

        // GET <EmployeesController>/5/picture
        [HttpGet("{id}/picture")]
        public async Task<ActionResult> GetPicture(int id)
        {
            (bool response, byte[] picture) = await this.employeeManagementService.TryGetPictureAsync(id);
            if (response)
            {
                return this.File(picture, "image/png");
            }

            return this.NotFound();
        }

        // DELETE <EmployeesController>/5/picture
        [HttpDelete("{id}/picture")]
        public async Task<ActionResult> DeletePicture(int id)
        {
            if (await this.employeeManagementService.DeletePictureAsync(id))
            {
                return this.Ok();
            }

            return this.NotFound();
        }
    }
}
