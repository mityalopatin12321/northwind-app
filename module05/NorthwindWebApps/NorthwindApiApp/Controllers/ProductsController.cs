﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Northwind.Services.Products;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NorthwindApiApp.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly IProductManagementService productManagementService;

        public ProductsController(IProductManagementService productManagementService)
        {
            this.productManagementService = productManagementService;
        }


        [HttpGet]
        public IAsyncEnumerable<Product> Get()
        {
            return this.productManagementService.GetProductsAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            (bool response, Product product) = await this.productManagementService.TryGetProductAsync(id);
            if (response)
            {
                return this.Ok(product);
            }

            return this.NotFound();
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] Product value)
        {
            if (value is null)
            {
                return this.BadRequest();
            }

            await this.productManagementService.CreateProductAsync(value);

            return this.Ok();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put([FromRoute] int id, [FromBody] Product value)
        {
            if (value is null)
            {
                return this.BadRequest();
            }

            if (await this.productManagementService.UpdateProductAsync(id, value))
            {
                return this.Ok();
            }

            return this.NotFound();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (await this.productManagementService.DeleteProductAsync(id))
            {
                return this.Ok();
            }

            return this.NotFound();
        }
    }
}
