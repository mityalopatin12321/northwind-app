﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Northwind.Services.Blogging;
using Northwind.Services.Employees;
using Northwind.Services.Products;

using NorthwindApiApp.ViewModels;

namespace NorthwindApiApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ArticlesController : Controller
    {
        private readonly IBloggingService bloggingService;
        private readonly IBlogCommentingService blogCommentingService;
        private readonly IEmployeeManagementService employeeManagementService;
        private readonly IProductManagementService productManagementService;

        public ArticlesController(IBloggingService bloggingService, IEmployeeManagementService employeeManagementService, IProductManagementService productManagementService, IBlogCommentingService blogCommentingService)
        {
            this.bloggingService = bloggingService;
            this.employeeManagementService = employeeManagementService;
            this.productManagementService = productManagementService;
            this.blogCommentingService = blogCommentingService;
        }

        // GET: Articles/Index
        [HttpGet]
        public ActionResult Index()
        {
            var model = bloggingService.GetBlogArticleAsync();
            return View(GetModels());

            async IAsyncEnumerable<BlogArticleViewModel> GetModels()
            {
                await foreach (var article in model)
                {
                    (_, var Employee) = await employeeManagementService.TryGetEmployeeAsync(article.EmployeeId);
                    var name = Employee?.FirstName;
                    yield return new BlogArticleViewModel()
                    {
                        BlogArticleId = article.BlogArticleId,
                        Content = article.Content,
                        EmployeeId = article.EmployeeId,
                        PublishDate = article.PublishDate,
                        Title = article.Title,
                        AuthorName = name,
                    };
                }
            }
        }

        // GET: Articles/Details/5
        [HttpGet("details/{id}")]
        public async Task<ActionResult> Details(int id)
        {
            (var flag, var article) = await bloggingService.TryGetBlogArticleAsync(id);
            if (!flag)
            {
                return this.NotFound();
            }

            (_, var Employee) = await employeeManagementService.TryGetEmployeeAsync(article.EmployeeId);
            var name = Employee?.FirstName;

            return View(new BlogArticleViewModel()
            {
                BlogArticleId = article.BlogArticleId,
                Content = article.Content,
                EmployeeId = article.EmployeeId,
                PublishDate = article.PublishDate,
                Title = article.Title,
                AuthorName = name,
            });
        }

        // GET: Articles/Create
        [HttpGet("Create")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Articles/Create
        [HttpPost("Create")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([FromForm] ArticleFormViewModel articleForm)
        {
            if (ModelState.IsValid)
            {
                (var flag, _) = await employeeManagementService.TryGetEmployeeAsync(articleForm.EmployeeId);
                if (!flag)
                {
                    return this.BadRequest();
                }

                BlogArticle blogArticle = new BlogArticle()
                {
                    Title = articleForm.Title,
                    Content = articleForm.Content,
                    EmployeeId = articleForm.EmployeeId,
                };

                var result = await bloggingService.CreateBlogArticleAsync(blogArticle);
                if (result <= 0)
                {
                    return this.StatusCode(500);
                }

                return RedirectToAction("Index");
            }

            return View();
        }

        // GET: Articles/Edit/5
        [HttpGet("Edit/{id}")]
        public async Task<ActionResult> Edit(int id)
        {
            (var flag, var article) = await bloggingService.TryGetBlogArticleAsync(id);
            if (!flag)
            {
                return this.NotFound();
            }

            return View(new ArticleEditFormViewModel()
            {
                Content = article.Content,
                Title = article.Title,
            });
        }

        // POST: Articles/Edit/5
        [HttpPost("Edit/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(int id, [FromForm] ArticleEditFormViewModel article)
        {
            if (ModelState.IsValid)
            {
                BlogArticle blogArticle = new BlogArticle()
                {
                    Title = article.Title,
                    Content = article.Content,
                };

                var result = await bloggingService.UpdateBlogArticleAsync(id, blogArticle);
                if (!result)
                {
                    return this.StatusCode(500);
                }

                return RedirectToAction("Index");
            }

            return View();
        }

        // POST: Articles/Delete/5
        [HttpPost("Delete/{id}")]
        public async Task<ActionResult> Delete([FromRoute] int id)
        {
            var flag = await this.bloggingService.DeleteBlogArticleAsync(id);

            if (!flag)
            {
                return this.NotFound();
            }

            return RedirectToAction("Index");
        }



        [HttpGet("details/{articleId}/products")]
        public async Task<ActionResult> RelatedProducts([FromRoute] int articleId)
        {
            (var flag, var article) = await bloggingService.TryGetBlogArticleAsync(articleId);
            if (!flag)
            {
                return this.NotFound();
            }

            List<ProductViewModel> list = new List<ProductViewModel>();
            foreach (var link in article.BlogArticleProducts)
            {
                (var exist, var product) = await productManagementService.TryGetProductAsync(link.ProductId);

                if (!exist)
                {
                    continue;
                }

                list.Add(new ProductViewModel()
                {
                    ProductId = product.ProductId,
                    ProductName = product.ProductName,
                    UnitPrice = product.UnitPrice,
                    UnitsInStock = product.UnitsInStock,
                    UnitsOnOrder = product.UnitsOnOrder,
                });
            }

            return View(new BlogArticleProductsViewModel()
            {
                BlogArticleId = article.BlogArticleId,
                Title = article.Title,
                BlogProducts = list,
            });
        }

        [HttpPost("details/{articleId}/products/create")]
        public async Task<ActionResult> CreateLinkToProduct(int articleId,[FromForm] ProductLinkViewModel productLink)
        {
            var row = await bloggingService.LinkProductToBlogArticleAsync(articleId, productLink.ProductId);

            if(row<=0)
            {
                return this.BadRequest();
            }

            return RedirectToAction("Index");
        }

        [HttpPost("details/{articleId}/products/delete")]
        public async Task<ActionResult> DeleteLinkToProduct(int articleId, [FromForm] ProductLinkViewModel productLink)
        {
            var row = await bloggingService.DeleteProductToBlogArticleLinkAsync(articleId, productLink.ProductId);

            return RedirectToAction("Index");
        }

        [HttpGet("details/{articleId}/products/edit")]
        public ActionResult EditLinkToProducts(int articleId)
        {
            ViewBag.ArticleId = articleId;
            return View();
        }



        [HttpGet("details/{articleId}/comments")]
        public async Task<ActionResult> Comments([FromRoute] int articleId)
        {
            (var flag, var article) = await bloggingService.TryGetBlogArticleAsync(articleId);
            if (!flag)
            {
                return this.NotFound();
            }

            List<BlogCommentViewModel> list = new List<BlogCommentViewModel>();
            foreach (var comment in article.BlogComments)
            {
                list.Add(new BlogCommentViewModel()
                {
                    Text = comment.Text,
                    BlogArticleId = comment.BlogArticleId,
                    CommentId = comment.CommentId,
                });
            }

            return View(new BlogArticleCommentsViewModel()
            {
                BlogArticleId = articleId,
                Title = article.Title,
                BlogComments = list,
            });
        }

        [HttpGet("details/{articleId}/comments/create")]
        public ActionResult CreateComment(int articleId)
        {
            return View();
        }

        [HttpPost("details/{articleId}/comments/create")]
        public async Task<ActionResult> CreateComment(int articleId, [FromForm] CommentFormViewModel blogComment)
        {
            if (ModelState.IsValid)
            {
                BlogComment comment = new BlogComment()
                {
                    Text = blogComment.Text,
                    BlogArticleId = articleId,
                };

                var result = await blogCommentingService.CreateBlogCommentAsync(comment);
                if (result <= 0)
                {
                    return this.BadRequest();
                }

                return RedirectToAction("Index");
            }

            return View();
        }

        [HttpGet("details/{articleId}/comments/edit/{id}")]
        public async Task<ActionResult> EditComment(int articleId,int id)
        {
            (var flag, var comment) = await blogCommentingService.TryGetBlogCommentAsync(id);
            if (!flag)
            {
                return this.NotFound();
            }

            return View(new CommentFormViewModel()
            {
                Text = comment.Text,
            });
        }

        [HttpPost("details/{articleId}/comments/edit/{id}")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditComment(int articleId, int id, [FromForm] CommentFormViewModel commentForm)
        {
            if (ModelState.IsValid)
            {
                BlogComment comment = new BlogComment()
                {
                    Text = commentForm.Text,
                };

                var result = await blogCommentingService.UpdateBlogCommentAsync(id, comment);
                if (!result)
                {
                    return this.StatusCode(500);
                }

                return RedirectToAction("Index");
            }

            return View();
        }
    }
}
