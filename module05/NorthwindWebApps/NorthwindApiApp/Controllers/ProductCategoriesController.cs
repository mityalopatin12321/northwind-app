﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Northwind.Services.Products;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NorthwindApiApp.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductCategoriesController : ControllerBase
    {
        private readonly IProductCategoriesManagementService productManagementService;

        public ProductCategoriesController(IProductCategoriesManagementService productManagementService)
        {
            this.productManagementService = productManagementService;
        }

        // GET: <ProductCategoriesController>
        [HttpGet]
        public IAsyncEnumerable<ProductCategory> Get()
        {
            return this.productManagementService.GetCategoriesAsync();
        }

        // GET <ProductCategoriesController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            (bool response, ProductCategory category) = await this.productManagementService.TryGetCategoryAsync(id);
            if (response)
            {
                return this.Ok(category);
            }

            return this.NotFound();
        }

        // POST <ProductCategoriesController>
        [HttpPost]
        public async Task<ActionResult> Post(ProductCategory category)
        {
            if (category is null)
            {
                return this.BadRequest();
            }

            await this.productManagementService.CreateCategoryAsync(category);

            return this.Ok();
        }

        // PUT <ProductCategoriesController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put([FromRoute] int id, [FromBody] ProductCategory category)
        {
            if (category is null)
            {
                return this.BadRequest();
            }

            if (await this.productManagementService.UpdateCategoryAsync(id, category))
            {
                return this.Ok();
            }

            return this.NotFound();
        }

        // DELETE <ProductCategoriesController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            if (await this.productManagementService.DeleteCategoryAsync(id))
            {
                return this.Ok();
            }

            return this.NotFound();
        }

        // PUT <ProductCategoriesController>/5/picture
        [HttpPut("{id}/picture")]
        public async Task<ActionResult> PutPicture(int id, IFormFile image)
        {
            if (image is null)
            {
                return this.BadRequest();
            }

            if (await this.productManagementService.UpdatePictureAsync(id, image.OpenReadStream()))
            {
                return this.Ok();
            }

            return this.NotFound();
        }

        // GET <ProductCategoriesController>/5/picture
        [HttpGet("{id}/picture")]
        public async Task<ActionResult> GetPicture(int id)
        {
            (bool response, byte[] picture) = await this.productManagementService.TryGetPictureAsync(id);
            if (response)
            {
                return this.File(picture, "image/png");
            }

            return this.NotFound();
        }

        // DELETE <ProductCategoriesController>/5/picture
        [HttpDelete("{id}/picture")]
        public async Task<ActionResult> DeletePicture(int id)
        {
            if (await this.productManagementService.DeletePictureAsync(id))
            {
                return this.Ok();
            }

            return this.NotFound();
        }
    }
}
