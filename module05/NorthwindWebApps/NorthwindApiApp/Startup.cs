using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore;

using Northwind.Services.Employees;
using Northwind.Services.Products;
using Northwind.Services.EntityFrameworkCore.Context;
using Northwind.Services.SQL;
using Northwind.Services.SQL.Products;
using Northwind.Services.SQL.Employees;

using Northwind.Services.EntityFrameworkCore.Blogging.Context;
using Northwind.Services.SQL.Blogging;
using Northwind.Services.Blogging;

using AutoMapper;

namespace NorthwindApiApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            this.AppConfiguration = configuration;
        }

        public IConfiguration AppConfiguration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            
            services.AddControllers();

            services.AddDbContext<NorthwindContext>(opt =>
                opt.UseSqlServer(AppConfiguration.GetConnectionString("SqlConnection")));
            services.AddDbContext<BloggingContext>(opt =>
                opt.UseSqlServer(AppConfiguration.GetConnectionString("NorthwindBloggingConnection")));
            
            services.AddScoped((asd) =>
                new MapperConfiguration
                (mc =>
                {
                    mc.AddProfile(new MappingProfile());
                }).CreateMapper());
            services.AddScoped<IEmployeeManagementService, EmployeeManagmentService>();
            services.AddScoped<IProductCategoriesManagementService, ProductCategoriesManagementService>();
            services.AddScoped<IProductManagementService, ProductManagementService>();
            services.AddScoped<IBlogCommentingService, BlogCommentingService>();
            services.AddScoped<IBloggingService, BloggingService>();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
