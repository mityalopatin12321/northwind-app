﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NorthwindApiApp.ViewModels
{
    public class BlogCommentViewModel
    {
        public int CommentId { get; set; }

        public string Text { get; set; }

        public int BlogArticleId { get; set; }
    }
}
