﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NorthwindApiApp.ViewModels
{
    public class BlogArticleCommentsViewModel
    {
        public int BlogArticleId { get; set; }

        public string Title { get; set; }

        public ICollection<BlogCommentViewModel> BlogComments { get; set; }
    }
}
