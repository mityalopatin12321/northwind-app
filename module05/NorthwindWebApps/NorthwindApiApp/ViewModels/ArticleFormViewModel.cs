﻿using System.ComponentModel.DataAnnotations;

namespace NorthwindApiApp.ViewModels
{
    public class ArticleFormViewModel
    {
        [Required(ErrorMessage = "Please enter blog title")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please enter blog text")]
        public string Content { get; set; }

        [Required(ErrorMessage = "Please enter employee id")]
        public int EmployeeId { get; set; }
    }
}
