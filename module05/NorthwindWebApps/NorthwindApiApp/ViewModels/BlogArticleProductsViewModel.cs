﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NorthwindApiApp.ViewModels
{
    public class BlogArticleProductsViewModel
    {
        public int BlogArticleId { get; set; }

        public string Title { get; set; }

        public ICollection<ProductViewModel> BlogProducts { get; set; }
    }
}
