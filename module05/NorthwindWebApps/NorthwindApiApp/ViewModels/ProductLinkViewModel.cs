﻿using System.ComponentModel.DataAnnotations;

namespace NorthwindApiApp.ViewModels
{
    public class ProductLinkViewModel
    {
        [Required(ErrorMessage = "Please enter product id")]
        public int ProductId { get; set; }
    }
}
