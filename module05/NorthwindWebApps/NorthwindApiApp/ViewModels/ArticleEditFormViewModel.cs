﻿using System.ComponentModel.DataAnnotations;

namespace NorthwindApiApp.ViewModels
{
    public class ArticleEditFormViewModel
    {
        [Required(ErrorMessage = "Please enter blog title")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please enter blog text")]
        public string Content { get; set; }
    }
}
