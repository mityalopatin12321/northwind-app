﻿using System.ComponentModel.DataAnnotations;

namespace NorthwindApiApp.ViewModels
{
    public class CommentFormViewModel
    {
        [Required(ErrorMessage = "Please enter your comment")]
        public string Text { get; set; }
    }
}
