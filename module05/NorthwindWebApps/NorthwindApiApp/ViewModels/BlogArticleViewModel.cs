﻿using System;
using System.Collections.Generic;

namespace NorthwindApiApp.ViewModels
{
    public class BlogArticleViewModel
    {
        public int BlogArticleId { get; set; }

        public string Title { get; set; }

        public string Content { get; set; }

        public DateTime PublishDate { get; set; }

        public int EmployeeId { get; set; }

        public string AuthorName { get; set; }
    }
}
